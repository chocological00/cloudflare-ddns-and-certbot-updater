# Cloudflare DDNS and Certbot updater

Does various things so that I don't have to update fields in Cloudflare's DNS panel manually.

## Features

### DDNS

Allows using Cloudflare as Dynamic DNS - Useful when running a server behind dynamically assigned IP.

Supports both IPv4 and IPv6.

### Certbot renew (DNS)

Automatically renews HTTPS certificates using Certbot, using DNS TXT challenges.

### Discord error reporting

Sends message via Discord when anything goes wrong.
